<?php
/**
 * Created by PhpStorm.
 * User: Yusupovb
 * Date: 27.07.2018
 * Time: 21:34
 */

namespace App\Form;

use App\Entity\UserGroup;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserType extends AbstractType
{
    private $passwordEncoder;

    function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('username', TextType::class)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('middlename', TextType::class)
            ->add('group', EntityType::class, array(
                'class' => UserGroup::class,
                'choice_label' => 'name',
                'placeholder' => 'Выберите группу',
            ))
            ->add('plainPassword', PasswordType::class, array(
                'required' => false
            ))
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)
        {
            $data = $event->getData();

            $password = $this->passwordEncoder->encodePassword($data, $data->getPlainPassword());
            $data->setPassword($password);

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}