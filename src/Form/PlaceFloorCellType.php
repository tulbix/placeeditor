<?php

namespace App\Form;

use App\Entity\PlaceFloorCell;
use App\Entity\Subobject;
use App\Repository\SubobjectRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceFloorCellType extends AbstractType
{
    private $subobjectRepository;

    function __construct(SubobjectRepository $subobjectRepository)
    {
        $this->subobjectRepository = $subobjectRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('subobject', EntityType::class, array(
                'class' => Subobject::class,
                'choice_label' => 'type.name',
                'placeholder' => 'Выберите тип',
            ))
            ->add('coord', CollectionType::class)
            ->add('size', CollectionType::class)
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event)
        {
            $data = $event->getData();

            $coord = $data->getCoord();
            if ($coord === null) {
                $data->setCoord([0, 0]);
            }

            $size = $data->getSize();
            if ($size === null) {
                $data->setSize([0, 0]);
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)
        {
            $data = $event->getData();

            if ($data->getId()) {
                $data->setUpdatedAt(new \DateTime());
            } else {
                $data->setCreatedAt(new \DateTime());
                $data->setUpdatedAt(new \DateTime());
            }

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlaceFloorCell::class,
            'csrf_protection' => false,
        ]);
    }
}
