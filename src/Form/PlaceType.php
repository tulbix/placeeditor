<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Legal;
use App\Entity\LegalDetail;
use App\Entity\Place;
use App\Services\Geo\AddressParser;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceType extends AbstractType
{
    private $addressParser;

    function __construct(AddressParser $addressParser)
    {
        $this->addressParser = $addressParser;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('phone', TextType::class, [
                'required' => true,
                'attr' => ['data-mask' => '(999)-999-99-99', 'placeholder' => 'Номер в формате (999)-999-99-99']
            ])
            ->add('square')
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)
        {
            $data = $event->getData();

            if ($data->getId()) {
                $data->setUpdatedAt(new \DateTime());
            } else {
                $data->setCreatedAt(new \DateTime());
                $data->setUpdatedAt(new \DateTime());
            }

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Place::class,
        ]);
    }
}
