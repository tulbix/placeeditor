<?php

namespace App\Form;

use App\Entity\UserGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('roles', ChoiceType::class, array(
                'attr' => array('class' => 'chosen-select', 'data-placeholder' => 'Выберите разделы'),
                'multiple' => true,
                'choices'  => array(
                    'Главная' => 'ROLE_ADMIN',
                    'Заявки' => 'ROLE_ADMIN_REQUESTS',
                    'Задачи' => 'ROLE_ADMIN_TASKS',
                    'Звонки' => 'ROLE_ADMIN_CALLS',
                    'Сообщения' => 'ROLE_ADMIN_MESSAGES',
                    'Клиенты' => 'ROLE_ADMIN_CLIENTS',
                    'Склады' => 'ROLE_ADMIN_OBJECTS',
                    'Транзакции' => 'ROLE_ADMIN_PAYMENTS',
                    'Настройки' => 'ROLE_ADMIN_SETTINGS',
                    'Полный доступ' => 'ROLE_SUPER_ADMIN',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserGroup::class,
        ]);
    }
}
