<?php

namespace App\Form;

use App\Entity\PlaceFloor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceFloorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add('image', FileType::class, array('data_class' => null))
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)
        {
            $data = $event->getData();

            if ($data->getId()) {
                $data->setUpdatedAt(new \DateTime());
            } else {
                $data->setCreatedAt(new \DateTime());
                $data->setUpdatedAt(new \DateTime());
            }

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlaceFloor::class,
            'csrf_protection' => false,
        ]);
    }
}
