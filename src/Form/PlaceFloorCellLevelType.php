<?php

namespace App\Form;

use App\Entity\PlaceFloorCellLevel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceFloorCellLevelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('cell')
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event)
        {
            $data = $event->getData();

            if ($data->getId()) {
                $data->setUpdatedAt(new \DateTime());
            } else {
                $data->setCreatedAt(new \DateTime());
                $data->setUpdatedAt(new \DateTime());
            }

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlaceFloorCellLevel::class,
            'csrf_protection' => false,
        ]);
    }
}
