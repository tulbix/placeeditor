<?php

namespace App\Repository;

use App\Entity\PlaceFloor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlaceFloor|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceFloor|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceFloor[]    findAll()
 * @method PlaceFloor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceFloorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlaceFloor::class);
    }

    public function getFirst($place)
    {
        return $this->createQueryBuilder('placeFloor')
            ->andWhere('placeFloor.place = :val')
            ->setParameter('val', $place)
            ->orderBy('placeFloor.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getLast($place)
    {
        return $this->createQueryBuilder('placeFloor')
            ->andWhere('placeFloor.place = :val')
            ->andWhere('placeFloor.deleted_at != NULL')
            ->setParameter('val', $place)
            ->orderBy('placeFloor.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

//    /**
//     * @return PlaceFloor[] Returns an array of PlaceFloor objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaceFloor
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
