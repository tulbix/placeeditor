<?php

namespace App\Repository;

use App\Entity\PlaceFloorCell;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlaceFloorCell|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceFloorCell|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceFloorCell[]    findAll()
 * @method PlaceFloorCell[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceFloorCellRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlaceFloorCell::class);
    }

//    /**
//     * @return PlaceFloorCell[] Returns an array of PlaceFloorCell objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaceFloorCell
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
