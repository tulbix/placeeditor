<?php

namespace App\Repository;

use App\Entity\PlaceFloorCellLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlaceFloorCellLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceFloorCellLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceFloorCellLog[]    findAll()
 * @method PlaceFloorCellLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceFloorCellLogRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlaceFloorCellLog::class);
    }

//    /**
//     * @return PlaceFloorCellLog[] Returns an array of PlaceFloorCellLog objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaceFloorCellLog
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
