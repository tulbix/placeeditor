<?php

namespace App\Repository;

use App\Entity\SubobjectType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubobjectType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubobjectType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubobjectType[]    findAll()
 * @method SubobjectType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubobjectTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SubobjectType::class);
    }

//    /**
//     * @return SubobjectTypeType[] Returns an array of SubobjectTypeType objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubobjectTypeType
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
