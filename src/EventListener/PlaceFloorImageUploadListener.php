<?php

namespace App\EventListener;

use Doctrine\ORM\Mapping\PostRemove;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Entity\PlaceFloor;
use App\Services\Files\PlaceFloorImageUploader;

class PlaceFloorImageUploadListener
{
    private $uploader;

    public function __construct(PlaceFloorImageUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof PlaceFloor) {
            return;
        }

        $fileName = $entity->getImage();

        $entity->setOldImage($fileName);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof PlaceFloor) {
            return;
        }

        $fileName = $entity->getImage();

        if ($fileName) {
            unlink($this->uploader->getTargetDirectory().'/'.$fileName);
        }
    }

    private function uploadFile($entity)
    {
        if (!$entity instanceof PlaceFloor) {
            return;
        }

        $file = $entity->getImage();

        if ($file instanceof UploadedFile) {
            $fileName = $this->uploader->upload($file);
            $entity->setImage($fileName);
        } elseif ($file instanceof File) {
            $fileName = $file->getFilename();
            $entity->setImage($fileName);
        }

        if ($oldFileName = $entity->getOldImage()) {
            if(isset($fileName) && $fileName != $oldFileName && file_exists($this->uploader->getTargetDirectory() . '/' . $oldFileName)) {
                unlink($this->uploader->getTargetDirectory() . '/' . $oldFileName);
            }
        }
    }
}