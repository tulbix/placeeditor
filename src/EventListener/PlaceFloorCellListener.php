<?php

namespace App\EventListener;

use App\Entity\PlaceFloorCell;
use App\Entity\PlaceFloorCellLog;
use App\Services\Place\PlaceManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PlaceFloorCellListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->saveCellLog($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->saveCellLog($args);
    }

    private function saveCellLog(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof PlaceFloorCell) {
            return;
        }

        $placeFloorCellLog = new PlaceFloorCellLog();
        $placeFloorCellLog->setCell($entity);
        $placeFloorCellLog->setCoord($entity->getCoord());
        $placeFloorCellLog->setSize($entity->getSize());
        $placeFloorCellLog->setCreatedAt(new \DateTime('now'));

        $em = $args->getEntityManager();
        $em->persist($placeFloorCellLog);
        $em->flush();
    }
}