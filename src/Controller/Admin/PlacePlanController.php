<?php

namespace App\Controller\Admin;

use App\Entity\Place;
use App\Entity\PlaceFloor;
use App\Repository\ClientOrderItemRepository;
use App\Repository\ClientOrderRepository;
use App\Repository\PlaceFloorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/objects/places/{id}/plan", name="admin-objects-places-plan")
 */
class PlacePlanController extends Controller
{
    /**
     * @Route("/{placeFloor}/{begin_at}/{end_at}", name="", methods="GET")
     */
    public function index(Place $place, PlaceFloor $placeFloor = null, $begin_at = 'now', $end_at = 'now', ClientOrderRepository $clientOrderRepository, ClientOrderItemRepository $clientOrderItemRepository): Response
    {
        $begin_at = new \DateTime($begin_at);
        $end_at = new \DateTime($end_at);
        $end_at->setTime(23,59,59);

        $placeFloors = $place->getPlaceFloors($begin_at);

        if ($placeFloor === null || !$placeFloors->contains($placeFloor)) {
            $placeFloor = $placeFloors[0];
        }

        $busyCellLevels = [];
        $ordersByPeriod = $clientOrderRepository->findAllPlaceOrdersInPeriod($place, $begin_at, $end_at);
        foreach ($ordersByPeriod as $order) {
            $orderItems = $clientOrderItemRepository->findBy(['clientOrder' => $order->getId()]);
            foreach ($orderItems as $item) {
                $busyCellLevels[] = $item->getCellLevel();
            }
        }

        return $this->render('admin/place_plan/index.html.twig', [
            'place' => $place,
            'placeFloor' => $placeFloor,
            'placeFloors' => $placeFloors,
            'begin_at' => $begin_at,
            'end_at' => $end_at,
            'busyCellLevels' => $busyCellLevels
        ]);
    }
}
