<?php

namespace App\Controller\Admin;

use App\Controller\Traits\FormValidation;
use App\Entity\Place;
use App\Entity\PlaceFloor;
use App\Entity\PlaceFloorCell;
use App\Entity\PlaceFloorCellLevel;
use App\Form\PlaceFloorCellLevelType;
use App\Form\PlaceFloorCellType;
use App\Form\PlaceFloorType;
use App\Repository\PlaceFloorRepository;
use App\Repository\PlaceRepository;
use App\Repository\SubobjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/objects/places/{id}/editor", name="admin-objects-places-editor")
 */
class PlaceEditorController extends Controller
{
    use FormValidation;

    /**
     * @Route("/{placeFloor}", name="", methods="GET")
     */
    public function index(Place $place, PlaceFloor $placeFloor = null, PlaceRepository $placeRepository, PlaceFloorRepository $placeFloorRepository, SubobjectRepository $subobjectRepository): Response
    {
        if ($placeFloor === null) {
            $placeFloor = $placeFloorRepository->getFirst($place);

            if ($placeFloor === null) {
                $placeFloor = new PlaceFloor();
                $placeFloor->setPlace($place);
                $placeFloor->setNumber(1);
                $placeFloor->setCreatedAt(new \DateTime('now'));
                $placeFloor->setUpdatedAt(new \DateTime('now'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($placeFloor);
                $em->flush();
            }
        }

        $defaultSubobject = $subobjectRepository->findOneBy([]);

        return $this->render('admin/place_editor/index.html.twig', [
            'place' => $place,
            'placeFloor' => $placeFloor,
            'places' => $placeRepository->findAll(),
            'defaultSubobject' => $defaultSubobject,
        ]);
    }

    /**
     * Создание этажа
     * @Route("/new", name="-floor-new", methods="POST")
     */
    public function createFloor(Place $place, Request $request): Response
    {
        $placeFloor = new PlaceFloor();
        $placeFloor->setPlace($place);

        $placeFloorForm = $this->createForm(PlaceFloorType::class, $placeFloor);
        $placeFloorForm->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if ($placeFloorForm->isSubmitted() && !$placeFloorForm->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($placeFloorForm)]));
            }

            if ($placeFloorForm->isSubmitted() && $placeFloorForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($placeFloor);
                $em->flush();

                $res = ['result' => 1, 'message' => 'ok'];
                return $response->setData(array_merge($res, ['id' => $placeFloor->getId()]));
            }
        }
    }

    /**
     * @Route("/{placeFloor}/delete", name="-floor-delete", methods="DELETE")
     */
    public function deleteFloor(Place $place, PlaceFloor $placeFloor): Response
    {
        $placeFloor->setDeletedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($placeFloor);
        $em->flush();

        $lastFloor = $place->getPlaceFloors()->last();

        $response = new JsonResponse();
        $res = ['result' => 1, 'message' => 'ok'];
        return $response->setData(array_merge($res, ['id' => $lastFloor->getId()]));
    }

    /**
     * @Route("/{placeFloor}/upload", name="-floor-upload", methods="GET|POST")
     */
    public function upload(PlaceFloor $placeFloor, Request $request): Response
    {
        $placeFloorForm = $this->createForm(PlaceFloorType::class, $placeFloor);
        $placeFloorForm->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if ($placeFloorForm->isSubmitted() && !$placeFloorForm->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($placeFloorForm)]));
            }

            if ($placeFloorForm->isSubmitted() && $placeFloorForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                $res  = ['result' => 1, 'message' => 'ok'];
                return $response->setData(array_merge($res, ['id' => $placeFloor->getId()]));
            }
        }
    }

    /**
     * @Route("/{placeFloor}/new", name="-cell-new", methods="GET|POST")
     */
    public function newCell(Place $place, PlaceFloor $placeFloor, Request $request): Response
    {
        $placeFloorCell = new PlaceFloorCell();
        $placeFloorCell->setFloor($placeFloor);

        $placeFloorCellForm = $this->createForm(PlaceFloorCellType::class, $placeFloorCell, [
            'action' => $this->generateUrl('admin-objects-places-editor-cell-new', [
                'id' => $place->getId(),
                'placeFloor' => $placeFloor->getId()
            ])
        ]);
        $placeFloorCellForm->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if ($placeFloorCellForm->isSubmitted() && !$placeFloorCellForm->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($placeFloorCellForm)]));
            }

            if ($placeFloorCellForm->isSubmitted() && $placeFloorCellForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($placeFloorCell);
                $em->flush();

                $placeFloorCellLevel = new PlaceFloorCellLevel();
                $placeFloorCellLevel->setCell($placeFloorCell);
                $placeFloorCellLevel->setName(1);
                $placeFloorCellLevel->setCreatedAt(new \DateTime('now'));
                $placeFloorCellLevel->setUpdatedAt(new \DateTime('now'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($placeFloorCellLevel);
                $em->flush();

                $res  = ['result' => 1, 'message' => 'ok'];
                return $response->setData(array_merge($res, [
                    'id' => $placeFloorCell->getId(),
                    'name' => $placeFloorCell->getName(),
                    'subobject_id' => $placeFloorCell->getSubobject()->getId(),
                    'color' => $placeFloorCell->getSubobject()->getColor(),
                    'level_id' => $placeFloorCellLevel->getId()
                ]));
            }
        }

        return $this->render('admin/place_editor/cell/new.html.twig', [
            'placeFloor' => $placeFloor,
            'form' => $placeFloorCellForm->createView(),
        ]);
    }

    /**
     * @Route("/{placeFloor}/{placeFloorCell}/edit", name="-cell-edit", methods="GET|POST")
     */
    public function editCell(Place $place, PlaceFloor $placeFloor, PlaceFloorCell $placeFloorCell, SubobjectRepository $subobjectRepository, Request $request): Response
    {
        if (!empty($request->query->get('place_floor_cell')['subobject'])) {
            $placeFloorCell->setSubobject($subobjectRepository->findOneBy(['id' => $request->query->get('place_floor_cell')['subobject']]));
        }

        $placeFloorCellForm = $this->createForm(PlaceFloorCellType::class, $placeFloorCell, [
            'attr' => ['id' => 'place_floor_cell_'.$placeFloorCell->getId()],
            'action' => $this->generateUrl('admin-objects-places-editor-cell-edit', [
                'id' => $place->getId(),
                'placeFloor' => $placeFloor->getId(),
                'placeFloorCell' => $placeFloorCell->getId()
            ])
        ]);
        $placeFloorCellForm->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if ($placeFloorCellForm->isSubmitted() && !$placeFloorCellForm->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($placeFloorCellForm)]));
            }

            if ($placeFloorCellForm->isSubmitted() && $placeFloorCellForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                $res  = ['result' => 1, 'message' => 'ok'];
                return $response->setData(array_merge($res, [
                    'id' => $placeFloorCell->getId(),
                    'subobject_id' => $placeFloorCell->getSubobject()->getId(),
                    'name' => $placeFloorCell->getName(),
                    'color' => $placeFloorCell->getSubobject()->getColor()
                ]));
            }
        }

        return $this->render('admin/place_editor/cell/edit.html.twig', [
            'placeFloorCell' => $placeFloorCell,
            'form' => $placeFloorCellForm->createView(),
        ]);
    }

    /**
     * @Route("/{placeFloor}/{placeFloorCell}/delete", name="-cell-delete", methods="DELETE")
     */
    public function deleteCell(PlaceFloorCell $placeFloorCell): Response
    {
        $response = new JsonResponse();

        $placeFloorCell->setDeletedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($placeFloorCell);
        $em->flush();

        $res = ['result' => 1, 'message' => 'ok'];
        return $response->setData($res);

        $res = ['result' => 0, 'message' => 'error'];
        return $response->setData($res);
    }

    /**
     * @Route("/{placeFloor}/{placeFloorCell}/new", name="-level-new", methods="GET|POST")
     */
    public function newLevel(PlaceFloorCell $placeFloorCell, Request $request): Response
    {
        $placeFloorCellLevel = new PlaceFloorCellLevel();
        $placeFloorCellLevel->setCell($placeFloorCell);

        $placeFloorCellLevelForm = $this->createForm(PlaceFloorCellLevelType::class, $placeFloorCellLevel);
        $placeFloorCellLevelForm->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if ($placeFloorCellLevelForm->isSubmitted() && !$placeFloorCellLevelForm->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($placeFloorCellLevelForm)]));
            }

            if ($placeFloorCellLevelForm->isSubmitted() && $placeFloorCellLevelForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($placeFloorCellLevel);
                $em->flush();

                $res  = ['result' => 1, 'message' => 'ok'];
                return $response->setData(array_merge($res, [
                    'id' => $placeFloorCellLevel->getId(),
                    'name' => $placeFloorCellLevel->getName(),
                ]));
            }
        }
    }

    /**
     * @Route("/{placeFloor}/{placeFloorCell}/{placeFloorCellLevel}/delete", name="-level-delete", methods="DELETE")
     */
    public function deleteLevel(PlaceFloorCellLevel $placeFloorCellLevel, Request $request): Response
    {
        $response = new JsonResponse();

        $placeFloorCellLevel->setDeletedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($placeFloorCellLevel);
        $em->flush();

        $res = ['result' => 1, 'message' => 'ok'];
        return $response->setData($res);
    }

    /**
     * @Route("/{placeFloor}/{placeFloorCell}/{placeFloorCellLevel}/edit", name="-level-edit", methods="GET|POST")
     */
    public function editLevel(PlaceFloorCellLevel $placeFloorCellLevel, Request $request): Response
    {
        $placeFloorCellLevelForm = $this->createForm(PlaceFloorCellLevelType::class, $placeFloorCellLevel);
        $placeFloorCellLevelForm->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if ($placeFloorCellLevelForm->isSubmitted() && !$placeFloorCellLevelForm->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($placeFloorCellLevelForm)]));
            }

            if ($placeFloorCellLevelForm->isSubmitted() && $placeFloorCellLevelForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                $res  = ['result' => 1, 'message' => 'ok'];
                return $response->setData(array_merge($res, [
                    'id' => $placeFloorCellLevel->getId(),
                    'name' => $placeFloorCellLevel->getName(),
                ]));
            }
        }
    }
}
