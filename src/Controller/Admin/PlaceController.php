<?php

namespace App\Controller\Admin;

use App\Controller\Traits\FormValidation;
use App\Entity\Place;
use App\Entity\PlaceFloor;
use App\Form\PlaceType;
use App\Repository\PlaceRepository;
use App\Services\Place\PlaceManager;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/objects/places", name="admin-objects-places")
 */
class PlaceController extends Controller
{
    use DataTablesTrait,
        FormValidation;

    /**
     * @Route("/", name="", methods="GET|POST")
     */
    public function index(Request $request): Response
    {
        $table = $this->createDataTable()
            ->add('id', TextColumn::class, ['label' => '#', 'className' => 'col-md-1'])
            ->add('name', TextColumn::class, ['label' => 'Наименование'])
            ->add('buttons', TwigColumn::class, [
                'label' => 'Действия',
                'className' => 'buttons col-lg-3 col-md-4',
                'template' => 'admin/place/buttonbar.html.twig',
            ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Place::class,
                'query' => function (QueryBuilder $builder) {
                    $builder
                        ->select('p')
                        ->from(Place::class, 'p')
                        ->andWhere("p.deleted_at IS NULL")
                    ;
                },
            ])->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('admin/place/index.html.twig', ['datatable' => $table]);
    }

    /**
     * @Route("/new", name="-new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $place = new Place();
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if (!$form->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($form)]));
            }

            // Do some stuff
            $res  = ['result' => 1, 'message' => 'ok'];
            return $response->setData(array_merge($res, ['data' => '']));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();

            $placeFloor = new PlaceFloor();
            $placeFloor->setPlace($place);
            $placeFloor->setNumber(1);
            $placeFloor->setCreatedAt(new \DateTime());
            $placeFloor->setUpdatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($placeFloor);
            $em->flush();

            return $this->redirectToRoute('admin-objects-places');
        }

        return $this->render('admin/place/new.html.twig', [
            'place' => $place,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="-edit", methods="GET|POST")
     */
    public function edit(Request $request, Place $place): Response
    {
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ( $request->isXmlHttpRequest() ) {
            $response = new JsonResponse();

            $res = ['result' => 0, 'message' => 'Invalid form'];
            if (!$form->isValid()) {
                return $response->setData(array_merge($res, ['data' => $this->getErrorMessages($form)]));
            }

            // Do some stuff
            $res  = ['result' => 1, 'message' => 'ok'];
            return $response->setData(array_merge($res, ['data' => '']));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin-objects-places');
        }

        return $this->render('admin/place/edit.html.twig', [
            'place' => $place,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="-delete", methods="DELETE")
     */
    public function delete(Request $request, Place $place): Response
    {
        if ($this->isCsrfTokenValid('delete'.$place->getId(), $request->request->get('_token'))) {
            $place->setDeletedAt(new \DateTime('now'));
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();
        }

        return $this->redirectToRoute('admin-objects-places');
    }
}
