<?php

namespace App\Controller\Traits;


trait FormValidation
{
    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        $name = $form->getName();
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$name . "[{$child->getName()}]"] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}