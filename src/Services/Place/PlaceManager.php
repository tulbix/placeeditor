<?php

namespace App\Services\Place;

use App\Entity\Place;
use App\Entity\PlaceFloorCell;
use App\Entity\Rate;
use App\Entity\SubobjectType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class PlaceManager
 * @package App\Services\Place
 */
class PlaceManager
{
    /**
     * Все ячейки объекта
     *
     * @param Place $place
     * @return Collection|PlaceFloorCell[]
     */
    public function getCells(Place $place): Collection
    {
        $placeCells = new ArrayCollection();

        $placeFloors = $place->getPlaceFloors();
        foreach ($placeFloors as $placeFloor) {
            $floorCells = $placeFloor->getPlaceFloorCells();
            foreach ($floorCells as $placeCell) {
                $placeCells[] = $placeCell;
            }
        }

        return $placeCells;
    }

    /**
     * Все места объекта
     *
     * @param Place $place
     * @return array|ArrayCollection
     */
    public function getLevels(Place $place): Collection
    {
        $placeLevels = new ArrayCollection();

        $placeCells = $this->getCells($place);
        foreach ($placeCells as $placeCell) {
            $cellLevels = $placeCell->getPlaceFloorCellLevels();
            foreach ($cellLevels as $placeLevel) {
                $placeLevels[] = $placeLevel;
            }
        }

        return $placeLevels;
    }

    /**
     * Вычисляем полезную площадь объекта
     *
     * @param Place $place
     * @return float
     */
    public function getUsefulSquare(Place $place)
    {
        $square = 0;

        $placeCells = $this->getCells($place);
        foreach ($placeCells as $placeCell) {
            $square += $placeCell->getSubobject()->getSquare() * $placeCell->getPlaceFloorCellLevels()->count();
        }

        return $square;
    }

    /**
     * Все типы используемые в объекте
     *
     * @param Place $place
     * @return Collection|SubobjectType[]
     */
    public function getTypes(Place $place): Collection
    {
        $placeTypes = new ArrayCollection();

        $placeCells = $this->getCells($place);
        foreach ($placeCells as $placeCell) {
            $placeType = $placeCell->getSubobject()->getType();
            if (!$placeTypes->contains($placeType)) {
                $placeTypes[] = $placeType;
            }
        }

        return $placeTypes;
    }

    /**
     * Все тарифы объекта
     *
     * @param Place $place
     * @return Collection|Rate[]
     */
    public function getRates(Place $place): Collection
    {
        $placeRates = new ArrayCollection();

        $placeTypes = $this->getTypes($place);
        foreach ($placeTypes as $placeType) {
            $placeRate = $placeType->getRate();
            if (!$placeRates->contains($placeRate)) {
                $placeRates[] = $placeRate;
            }
        }

        return $placeRates;
    }

    /**
     * Минимальная цена, исходя из документов тарифов
     *
     * @param Place $place
     * @return int
     */
    public function getMinPrice(Place $place)
    {
        $price = 0;

        $placeRates = $this->getRates($place);
        foreach ($placeRates as $placeRate) {
            $rateDocs = $placeRate->getDocs();
            foreach ($rateDocs as $rateDoc) {
                $docPrice = $rateDoc->getPrice();
                $docPrice = $docPrice - $docPrice*$rateDoc->getDiscount()/100;
                if ($price == 0 || $price > $docPrice) {
                    $price = $docPrice;
                }
            }
        }

        return $price;
    }

    /**
     * Максимальная цена, исходя из документов тарифов
     *
     * @param Place $place
     * @return int
     */
    public function getMaxPrice(Place $place)
    {
        $price = 0;

        $placeRates = $this->getRates($place);
        foreach ($placeRates as $placeRate) {
            $rateDocs = $placeRate->getDocs();
            foreach ($rateDocs as $rateDoc) {
                $docPrice = $rateDoc->getPrice();
                $docPrice = $docPrice - $docPrice*$rateDoc->getDiscount()/100;
                if ($price < $docPrice) {
                    $price = $docPrice;
                }
            }
        }

        return $price;
    }

    /**
     * Цена места на заданную дату
     *
     * @param PlaceFloorCell $placeFloorCell
     * @param \DateTime $orderDate
     * @return mixed
     */
    public function getCellPrice(PlaceFloorCell $placeFloorCell, \DateTime $orderDate) {
        $cellSquare = $placeFloorCell->getSubobject()->getSquare();
        $rateDocs = $placeFloorCell->getSubobject()->getType()->getRate()->getDocs();

        $cellRateDocs = new ArrayCollection();

        foreach ($rateDocs as $rateDoc) {
            if ($rateDoc->getBeginAt() <= $orderDate && $rateDoc->getEndAt() >= $orderDate) {
                $squareMin = $rateDoc->getSquareMin();
                $squareMin = ($squareMin > 0) ? $squareMin : 0;

                $squareMax = $rateDoc->getSquareMax();
                $squareMax = ($squareMax > 0) ? $squareMax : 99999999;

                if ($cellSquare >= $squareMin && $cellSquare <= $squareMax) {
                    $cellRateDocs[] = $rateDoc;
                }
            }
        }

        return $cellRateDocs->first();
    }
}