<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="sf_users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User implements UserInterface, \Serializable
{
    CONST SUPER_ADMIN = 1;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $middlename;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "Указанный email '{{ value }}' не является валидным.",
     *     checkMX = false
     * )
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserGroup", inversedBy="users")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $group;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="creator")
     */
    private $creatorTasks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="executor")
     */
    private $executorTasks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="sender")
     */
    private $messages;

    public function __construct()
    {
        $this->isActive = true;
        $this->creatorTasks = new ArrayCollection();
        $this->executorTasks = new ArrayCollection();
        $this->messages = new ArrayCollection();

        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;
    }

    public function getMiddlename()
    {
        return $this->middlename;
    }

    public function getFullname()
    {
        $fullname = [];
        if ($this->lastname) $fullname[] = $this->lastname;
        if ($this->firstname) $fullname[] = $this->firstname;
        if ($this->middlename) $fullname[] = $this->middlename;

        return (count($fullname) > 0) ? implode(' ', $fullname) : $this->username;
    }

    public function getShortname()
    {
        $fullname = [];
        if ($this->lastname) $fullname[] = $this->lastname;
        if ($this->firstname) $fullname[] = mb_strcut($this->firstname, 0, 2).'.';
        if ($this->middlename) $fullname[] = mb_strcut($this->middlename, 0, 2).'.';

        return (count($fullname) > 0) ? implode(' ', $fullname) : $this->username;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getisActive()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getGroup(): ?UserGroup
    {
        return $this->group;
    }

    public function setGroup(?UserGroup $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getRoles()
    {
        $group = $this->getGroup();
        $roles = (!empty($group) && !empty($group->getRoles())) ? $group->getRoles() : array('ROLE_ADMIN', 'ROLE_ADMIN_SETTINGS'); // !!! ROLE_ADMIN_SETTINGS потом убрать !!!
        return (in_array('ROLE_ADMIN', $roles) || in_array('ROLE_SUPER_ADMIN', $roles)) ? $roles : array_merge($roles, ['ROLE_ADMIN']);
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    /**
     * @return Collection|Task[]
     */
    public function getCreatorTasks(): Collection
    {
        return $this->creatorTasks;
    }

    /**
     * @return Collection|Task[]
     */
    public function getExecutorTasks(): Collection
    {
        return $this->executorTasks;
    }

    public function __toString()
    {
        return $this->getUsername();
    }


}
