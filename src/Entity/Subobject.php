<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sf_subobjects")
 * @ORM\Entity(repositoryClass="App\Repository\SubobjectRepository")
 */
class Subobject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\SubobjectType", inversedBy="subobjects")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false, onDelete="CASCADE"))
     */
    private $type;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $width;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $length;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $height;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceFloorCell", mappedBy="subobject")
     */
    private $placeFloorCells;

    public function __construct()
    {
        $this->placeFloorCells = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?SubobjectType
    {
        return $this->type;
    }

    public function setType(?SubobjectType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getSquare()
    {
        return $this->width * $this->length;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|PlaceFloorCell[]
     */
    public function getPlaceFloorCells(): Collection
    {
        return $this->placeFloorCells;
    }

    public function addPlaceFloorCell(PlaceFloorCell $placeFloorCell): self
    {
        if (!$this->placeFloorCells->contains($placeFloorCell)) {
            $this->placeFloorCells[] = $placeFloorCell;
            $placeFloorCell->setSubobject($this);
        }

        return $this;
    }

    public function removePlaceFloorCell(PlaceFloorCell $placeFloorCell): self
    {
        if ($this->placeFloorCells->contains($placeFloorCell)) {
            $this->placeFloorCells->removeElement($placeFloorCell);
            // set the owning side to null (unless already changed)
            if ($placeFloorCell->getSubobject() === $this) {
                $placeFloorCell->setSubobject(null);
            }
        }

        return $this;
    }
}
