<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sf_places_floors")
 * @ORM\Entity(repositoryClass="App\Repository\PlaceFloorRepository")
 */
class PlaceFloor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Place", inversedBy="placeFloors")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=false, onDelete="CASCADE"))
     */
    private $place;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^\d+/",
     *     message="Поле должно содержать только числовое значение"
     * )
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File()
     */
    private $image;

    private $oldImage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceFloorCell", mappedBy="floor")
     */
    private $placeFloorCells;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function __construct()
    {
        $this->placeFloorCells = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getOldImage()
    {
        return $this->oldImage;
    }

    public function setOldImage($oldImage): self
    {
        $this->oldImage = $oldImage;

        return $this;
    }

    /**
     * @return Collection|PlaceFloorCell[]
     */
    public function getPlaceFloorCells(\DateTime $date = null): Collection
    {
        if ($date === null) {
            $date = new \DateTime('now');
        } else {
            list($hours, $minutes, $seconds) = explode(':', date('H:i:s'));
            $date->setTime($hours, $minutes, $seconds);
        }

        return $this->placeFloorCells->filter(function ($cell) use ($date) {
            return ($cell->getDeletedAt() > $date || $cell->getDeletedAt() == null) && $cell->getCreatedAt() <= $date;
        });
    }

    public function addPlaceFloorCell(PlaceFloorCell $placeFloorCell): self
    {
        if (!$this->placeFloorCells->contains($placeFloorCell)) {
            $this->placeFloorCells[] = $placeFloorCell;
            $placeFloorCell->setFloor($this);
        }

        return $this;
    }

    public function removePlaceFloorCell(PlaceFloorCell $placeFloorCell): self
    {
        if ($this->placeFloorCells->contains($placeFloorCell)) {
            $this->placeFloorCells->removeElement($placeFloorCell);
            // set the owning side to null (unless already changed)
            if ($placeFloorCell->getFloor() === $this) {
                $placeFloorCell->setFloor(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
