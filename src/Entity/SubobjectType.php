<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sf_subobjects_types")
 * @ORM\Entity(repositoryClass="App\Repository\SubobjectTypeRepository")
 */
class SubobjectType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Rate", inversedBy="types")
     * @ORM\JoinColumn(name="rate_id", referencedColumnName="id", nullable=false, onDelete="CASCADE"))
     */
    private $rate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subobject", mappedBy="type")
     */
    private $subobjects;

    public function __construct()
    {
        $this->subobjects = new ArrayCollection();
    }

    /**
     * @return Collection|Subobject[]
     */
    public function getSubobjects(): Collection
    {
        return $this->subobjects;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRate(): ?Rate
    {
        return $this->rate;
    }

    public function setRate(?Rate $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}
