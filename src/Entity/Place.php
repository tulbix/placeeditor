<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sf_places")
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
 */
class Place
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank(
     *     message="Адрес не указан или содержит ошибки"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $square;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceFloor", mappedBy="place")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    private $placeFloors;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function __construct()
    {
        $this->placeFloors = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getSquare()
    {
        return $this->square;
    }

    public function setSquare($square): self
    {
        $this->square = $square;

        return $this;
    }

    /**
     * @return Collection|PlaceFloor[]
     */
    public function getPlaceFloors(\DateTime $date = null): Collection
    {
        if ($date === null) {
            $date = new \DateTime('now');
        } else {
            list($hours, $minutes, $seconds) = explode(':', date('H:i:s'));
            $date->setTime($hours, $minutes, $seconds);
        }

        return $this->placeFloors->filter(function ($floor) use ($date) {
            return ($floor->getDeletedAt() > $date || $floor->getDeletedAt() == null) && $floor->getCreatedAt() <= $date;
        });
    }

    public function addPlaceFloor(PlaceFloor $placeFloor): self
    {
        if (!$this->placeFloors->contains($placeFloor)) {
            $this->placeFloors[] = $placeFloor;
            $placeFloor->setPlace($this);
        }

        return $this;
    }

    public function removePlaceFloor(PlaceFloor $placeFloor): self
    {
        if ($this->placeFloors->contains($placeFloor)) {
            $this->placeFloors->removeElement($placeFloor);
            // set the owning side to null (unless already changed)
            if ($placeFloor->getPlace() === $this) {
                $placeFloor->setPlace(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
