<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sf_places_floors_cells")
 * @ORM\Entity(repositoryClass="App\Repository\PlaceFloorCellRepository")
 */
class PlaceFloorCell
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaceFloor", inversedBy="placeFloorCells")
     * @ORM\JoinColumn(name="floor_id", referencedColumnName="id", nullable=false, onDelete="CASCADE"))
     */
    private $floor;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Subobject", inversedBy="placeFloorCells")
     * @ORM\JoinColumn(name="subobject_id", referencedColumnName="id", nullable=false, onDelete="CASCADE"))
     */
    private $subobject;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="json_array")
     */
    private $coord;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="json_array")
     */
    private $size;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceFloorCellLevel", mappedBy="cell")
     */
    private $placeFloorCellLevels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceFloorCellLog", mappedBy="cell")
     */
    private $placeFloorCellLogs;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function __construct()
    {
        $this->placeFloorCellLevels = new ArrayCollection();
        $this->placeFloorCellLogs = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFloor(): ?PlaceFloor
    {
        return $this->floor;
    }

    public function setFloor(?PlaceFloor $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function getSubobject(): ?Subobject
    {
        return $this->subobject;
    }

    public function setSubobject(?Subobject $subobject): self
    {
        $this->subobject = $subobject;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCoord()
    {
        return $this->coord;
    }

    public function setCoord($coord): self
    {
        $this->coord = $coord;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return Collection|PlaceFloorCellLevel[]
     */
    public function getPlaceFloorCellLevels(\DateTime $date = null): Collection
    {
        if ($date === null) {
            $date = new \DateTime('now');
        } else {
            list($hours, $minutes, $seconds) = explode(':', date('H:i:s'));
            $date->setTime($hours, $minutes, $seconds);
        }

        return $this->placeFloorCellLevels->filter(function ($level) use ($date) {
            return ($level->getDeletedAt() > $date || $level->getDeletedAt() == null) && $level->getCreatedAt() <= $date;
        });
    }

    public function addPlaceFloorCellLevel(PlaceFloorCellLevel $placeFloorCellLevel): self
    {
        if (!$this->placeFloorCellLevels->contains($placeFloorCellLevel)) {
            $this->placeFloorCellLevels[] = $placeFloorCellLevel;
            $placeFloorCellLevel->setCell($this);
        }

        return $this;
    }

    public function removePlaceFloorCellLevel(PlaceFloorCellLevel $placeFloorCellLevel): self
    {
        if ($this->placeFloorCellLevels->contains($placeFloorCellLevel)) {
            $this->placeFloorCellLevels->removeElement($placeFloorCellLevel);
            // set the owning side to null (unless already changed)
            if ($placeFloorCellLevel->getCell() === $this) {
                $placeFloorCellLevel->setCell(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlaceFloorCellLog[]
     */
    public function getPlaceFloorCellLogs(\DateTime $date = null): Collection
    {
        if ($date === null) {
            $date = new \DateTime('now');
        } else {
            list($hours, $minutes, $seconds) = explode(':', date('H:i:s'));
            $date->setTime($hours, $minutes, $seconds);
        }

        return $this->placeFloorCellLogs->filter(function ($log) use ($date) {
            return $log->getCreatedAt() <= $date;
        });
    }

    /**
     * Получаем последнее состояние ячейки на заданную дату
     */
    public function getLastCellLog(\DateTime $date = null)
    {
        return $this->getPlaceFloorCellLogs($date)->last();
    }

    public function addPlaceFloorCellLog(PlaceFloorCellLog $placeFloorCellLog): self
    {
        if (!$this->placeFloorCellLogs->contains($placeFloorCellLog)) {
            $this->placeFloorCellLogs[] = $placeFloorCellLog;
            $placeFloorCellLog->setCell($this);
        }

        return $this;
    }

    public function removePlaceFloorCellLog(PlaceFloorCellLog $placeFloorCellLog): self
    {
        if ($this->placeFloorCellLogs->contains($placeFloorCellLog)) {
            $this->placeFloorCellLogs->removeElement($placeFloorCellLog);
            // set the owning side to null (unless already changed)
            if ($placeFloorCellLog->getCell() === $this) {
                $placeFloorCellLog->setCell(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
