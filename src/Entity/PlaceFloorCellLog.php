<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="sf_places_floors_cells_logs")
 * @ORM\Entity(repositoryClass="App\Repository\PlaceFloorCellLogRepository")
 */
class PlaceFloorCellLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaceFloorCell", inversedBy="placeFloorCellLogs")
     * @ORM\JoinColumn(name="cell_id", referencedColumnName="id", nullable=false, onDelete="CASCADE"))
     */
    private $cell;

    /**
     * @ORM\Column(type="json_array")
     */
    private $coord;

    /**
     * @ORM\Column(type="json_array")
     */
    private $size;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function getId()
    {
        return $this->id;
    }

    public function getCell(): ?PlaceFloorCell
    {
        return $this->cell;
    }

    public function setCell(?PlaceFloorCell $cell): self
    {
        $this->cell = $cell;

        return $this;
    }

    public function getCoord()
    {
        return $this->coord;
    }

    public function setCoord($coord): self
    {
        $this->coord = $coord;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
