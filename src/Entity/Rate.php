<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sf_rates")
 * @ORM\Entity(repositoryClass="App\Repository\RateRepository")
 */
class Rate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubobjectType", mappedBy="rate")
     */
    private $types;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RateDoc", mappedBy="rate")
     */
    private $docs;

    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->docs = new ArrayCollection();
    }

    /**
     * @return Collection|SubobjectType[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|RateDoc[]
     */
    public function getDocs(): Collection
    {
        return $this->docs;
    }

    public function addDoc(RateDoc $doc): self
    {
        if (!$this->docs->contains($doc)) {
            $this->docs[] = $doc;
            $doc->setRate($this);
        }

        return $this;
    }

    public function removeDoc(RateDoc $doc): self
    {
        if ($this->docs->contains($doc)) {
            $this->docs->removeElement($doc);
            // set the owning side to null (unless already changed)
            if ($doc->getRate() === $this) {
                $doc->setRate(null);
            }
        }

        return $this;
    }
}
