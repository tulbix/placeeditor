(function( $ ){
    var placeEditor = {

        $editor_url: $('#place_editor').data('editor_url'),

        cacheDom: function(){
            this.$body = $('body');
            this.$window = $(window);
        },

        floors: {
            preloaderShow: function() {
                $('#place_editor').addClass('core_loading');
            },

            preloaderHide: function() {
                $('#place_editor').removeClass('core_loading');
            },

            loadFloor: function(e) {
                e.stopImmediatePropagation();

                let that = this;
                let floor_id = $(e.currentTarget).data('floor_id');

                that.floors.loadFloorById(floor_id, that);
            },

            loadFloorById: function(floor_id, that) {
                $('#place_editor').data('floor_id', floor_id);

                $.ajax({
                    url: that.$editor_url + '/' + floor_id,
                    type: "GET",
                    beforeSend: function () {
                        that.floors.preloaderShow();
                    },
                    success: function(data) {
                        $('body').html($(data).find('.sidebar-content'));

                        $('#place_editor .cr_fg').resizable({disabled: false, containment: ".cr_figures"});
                        $('#place_editor .cr_fg').draggable({disabled: false, containment: ".cr_figures"});
                        $('[data-toggle="tooltip"]').tooltip();
                        that.levels.updateLevelsHeight(that);
                    }
                });
            },

            createFloor: function (e) {
                e.stopImmediatePropagation();

                if (!$(e.currentTarget).hasClass('clicked')) {

                    let that = this;
                    let lastFloor = $('#cr_floors .floor').last();
                    let last_floor_number = lastFloor.data('floor_number') + 1;

                    $.ajax({
                        url: that.$editor_url + '/new',
                        data: {"place_floor[number]": last_floor_number},
                        type: "POST",
                        beforeSend: function () {
                            that.floors.preloaderShow();
                            $(e.currentTarget).addClass('clicked');
                        },
                        success: function (data) {
                            $(e.currentTarget).removeClass('clicked');
                            if (data.result === 1) {
                                that.floors.loadFloorById(data.id, that);
                            } else {
                                that.floors.preloaderHide();
                            }
                        }
                    });
                }
            },

            deleteFloor: function (e) {
                e.stopImmediatePropagation();

                if (!$(e.currentTarget).hasClass('clicked')) {

                    let that = this;
                    let floor_id = $('#place_editor').data('floor_id');
                    let token = $('#place_editor').data('token');
                    $.ajax({
                        url: that.$editor_url + '/' + floor_id + '/delete',
                        type: "DELETE",
                        beforeSend: function () {
                            that.floors.preloaderShow();
                            $(e.currentTarget).addClass('clicked');
                        },
                        success: function (data) {
                            $(e.currentTarget).removeClass('clicked');
                            if (data.result === 1) {
                                that.floors.loadFloorById(data.id, that);
                            } else {
                                that.floors.preloaderHide();
                            }
                        }
                    });
                }
            },

            selectImage: function (e) {
                e.stopImmediatePropagation();

                let that = this;
                let floor_id = $('#place_editor').data('floor_id');
                let fileInput = $('#floorImageUploadForm input[name="place_floor[image]"]')[0];

                $(fileInput).click();
                $(fileInput).on('change', function () {
                    if (!$(e.currentTarget).hasClass('clicked')) {
                        $.ajax({
                            url: that.$editor_url + '/' + floor_id + '/upload',
                            data: new FormData($("#floorImageUploadForm")[0]),
                            dataType:'json',
                            type: "POST",
                            contentType: false,
                            processData: false,
                            beforeSend: function () {
                                that.floors.preloaderShow();
                                $(e.currentTarget).addClass('clicked');
                            },
                            success: function (data) {
                                $(e.currentTarget).removeClass('clicked');
                                if (data.result === 1) {
                                    that.floors.loadFloorById(data.id, that);
                                } else {
                                    that.floors.preloaderHide();
                                }
                            }
                        });
                    }
                });
            },
        },

        cells: {
            selectTool: function (e) {
                e.stopImmediatePropagation();

                let image = $('#place_editor .cr_figures').attr('src');
                if (image === undefined) {
                    $('#place_editor .cr_upload_bg').addClass("shake").delay(1000).queue(function(next){
                        $(this).removeClass("shake");
                        next();
                    });

                    toastr.options = {
                        closeButton: true,
                        progressBar: false,
                        showMethod: 'slideDown',
                        timeOut: 5000
                    };
                    toastr.warning('Для начала необходимо загрузить изображение плана текущего этажа.', 'Внимание!');
                }

                $(e.currentTarget).parent().find('.i').removeClass('active');
                $(e.currentTarget).addClass('active');
                $('#place_editor .cr_schema').removeClass('cr_cursor_move');
                $('#place_editor .cr_fg').draggable("option", "disabled", true);

                let tool = $(e.currentTarget).data('name');
                if (tool == 'selecting') {
                    $('#place_editor .cr_schema').addClass('cr_cursor_move');
                    $('#place_editor .cr_fg').draggable("option", "disabled", false);
                }
            },

            createCell: function(e) {
                e.stopImmediatePropagation();

                let tool = $('#place_editor .cr_navi .i.active').data('name');
                if (tool == 'celldrawing') {
                    let that = this;
                    let floor_id = $('#place_editor').data('floor_id');
                    let subobject_id = $('#place_editor').data('subobject_id');
                    let schemaPosition = $(e.currentTarget).offset();
                    let planPosition = $('#place_editor .cr_figures').position();
                    let left = e.pageX - schemaPosition.left - planPosition.left;
                    let top = e.pageY - schemaPosition.top - planPosition.top;
                    let $newCell = $('<div class="cr_fg" data-type="box" style="top: '+top+'px; left: '+left+'px; width: 80px; height: 80px;" ' +
                        'data-cell_id="" data-subobject_id="" data-name="New" data-type="box">' +
                                        '<div class="cr_tool" style="background-color: transparent;"></div>' +
                                        '<div class="cr_layer_box">' +
                                            '<div class="i" data-level_id=""></div>' +
                                        '</div>' +
                                        '<div class="cr_title hide">New</div>' +
                                    '</div>');
                    $('#place_editor .cr_cells').append($newCell);

                    $newCell.resizable({disabled: false});
                    $newCell.draggable({disabled: true});

                    $.ajax({
                        url: that.$editor_url + '/' + floor_id + '/new',
                        data: {
                            "place_floor_cell[name]": 'New',
                            "place_floor_cell[subobject]": subobject_id,
                            "place_floor_cell[coord][0]": left,
                            "place_floor_cell[coord][1]": top,
                            "place_floor_cell[size][0]": 80,
                            "place_floor_cell[size][1]": 80
                        },
                        type: "POST",
                        beforeSend: function () {
                            $newCell.addClass('element_loading');
                        },
                        success: function (data) {
                            $newCell.removeClass('element_loading');
                            if (data.result === 1) {
                                $newCell.attr('data-cell_id', data.id);
                                $newCell.attr('data-subobject_id', data.subobject_id);
                                $newCell.find('.cr_title').html(data.name);
                                $newCell.find('.cr_layer_box .i').attr('data-level_id', data.level_id);
                                $newCell.find('.cr_layer_box .i').html('1');
                                $newCell.find('.cr_tool').css('background-color', data.color);
                                $newCell.css('border-color', data.color);

                                $('#place_editor .cr_navi .i[data-name="selecting"]').click();

                                that.levels.updateLevelsHeight(that);
                                $newCell.click();
                            }
                        }
                    });
                }
            },

            selectCell: function(e) {
                e.stopImmediatePropagation();

                let that = this;
                let tool = $('#place_editor .cr_navi .i.active').data('name');
                if (tool == 'selecting') {
                    let floor_id = $('#place_editor').data('floor_id');
                    let cell_id = $(e.currentTarget).data('cell_id');
                    let url = (cell_id) ? that.$editor_url + '/' + floor_id + '/' + cell_id + '/edit' : that.$editor_url + '/' + floor_id + '/new';

                    $('#place_editor .cr_fg').not($(e.currentTarget)).removeClass('selected');
                    if (!$(e.currentTarget).hasClass('selected')) {
                        $(e.currentTarget).addClass('selected');

                        $.ajax({
                            url: url,
                            type: "GET",
                            beforeSend: function () {
                                $('#place_editor .core_object_schema_direct_block .col-xs-12').removeClass('col-xs-12').addClass('col-xs-9');
                                let sidebar_width = $('#place_editor .core_subobject_info').width() + 40;
                                $('#place_editor .cr_schema').css('width', 'calc(100vw - '+sidebar_width+'px)');
                                $('#place_editor .core_subobject_info').html('').removeClass('core_hide').addClass('core_loading');
                            },
                            success: function (data) {
                                $('#place_editor .core_subobject_info').html($(data)).removeClass('core_loading');
                            }
                        });
                    }
                }
            },

            unselectCell: function(e) {

                $('#place_editor .cr_fg.selected').removeClass('selected');
                $('#place_editor .core_object_schema_direct_block .col-xs-9').removeClass('col-xs-9').addClass('col-xs-12');
                $('#place_editor .cr_schema').css('width', '100vw');
                $('#place_editor .core_subobject_info').html('').addClass('core_hide');
            },

            saveCell: function(e) {
                e.preventDefault();

                let cell_id = $(e.currentTarget).data('cell_id');
                let $cell = $('.cr_fg[data-cell_id="'+cell_id+'"]');
                let form = $(e.currentTarget).parents('form');
                let action = form.attr('action');

                $.ajax({
                    url: action,
                    data: form.serialize(),
                    type: "POST",
                    beforeSend: function () {
                        $cell.addClass('element_loading');
                        form.parent().addClass('core_loading');
                    },
                    success: function(data) {
                        $cell.removeClass('element_loading');
                        form.parent().removeClass('core_loading');
                        $('div.form-group').removeClass('has-error');
                        $('div.form-group > span.help-block').remove();
                        if (data.result === 0) {
                            for (let key in data.data) {
                                let current_field = $(form.find('[name*="'+key+'"]')[0]);
                                current_field.closest('div.form-group').addClass('has-error');
                                $(form.find('[name*="'+key+'"]')[0]).after('<span class="help-block"><ul class="list-unstyled"><li>'+data.data[key]+'</li></ul></span>');
                            }
                        } else {
                            if (data.result === 1) {
                                $cell.attr('data-name', data.name);
                                $cell.attr('data-subobject_id', data.subobject_id);
                                $cell.find('.cr_title').html(data.name);
                                $cell.find('.cr_tool').css('background-color', data.color);
                                $cell.css('border-color', data.color);
                                $cell.find('.cr_layer_box .i').css('border-color', data.color);
                            }
                        }
                    }
                });
            },

            changeCellType: function(e) {
                e.preventDefault();

                let cell_id = $(e.currentTarget).data('cell_id');
                let form = $(e.currentTarget).parents('form');
                let action = form.attr('action');

                $.ajax({
                    url: action,
                    data: form.serialize(),
                    type: "GET",
                    beforeSend: function () {
                        $('#place_editor .core_subobject_info .subobject-info').addClass('element_loading');
                    },
                    success: function(data) {
                        console.log(data);
                        $('#place_editor .core_subobject_info .subobject-info').removeClass('element_loading');
                        $('#place_editor .core_subobject_info .subobject').replaceWith($(data).find('.subobject'));
                    }
                });
            },

            deleteCell: function (e) {
                e.preventDefault();

                if (confirm('Вы уверены, что хотите удалить эту ячейку?')) {

                    let cell_id = $(e.currentTarget).data('cell_id');
                    let $cell = $('.cr_fg[data-cell_id="' + cell_id + '"]');
                    let form = $('#deletePlaceFloorCellForm');
                    let action = form.attr('action');

                    $.ajax({
                        url: action,
                        data: form.serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $cell.addClass('element_loading');
                            form.parent().addClass('core_loading');
                        },
                        success: function (data) {
                            $cell.removeClass('element_loading');
                            form.parent().removeClass('core_loading');
                            if (data.result === 1) {
                                $cell.remove();

                                $('#place_editor .core_object_schema_direct_block .col-xs-9').removeClass('col-xs-9').addClass('col-xs-12');
                                $('#place_editor .cr_schema').css('width', '87vw');
                                $('#place_editor .core_subobject_info').html('').addClass('core_hide');
                            }
                        }
                    });
                }
            },

            deformCell: function (e) {
                let that = this;
                let floor_id = $('#place_editor').data('floor_id');
                let $cell = $(e.currentTarget);
                let schemaPosition = $cell.parents('.cr_figures_wrap').offset();
                let planPosition = $('#place_editor .cr_figures').position();
                let cellPosition = $cell.offset();
                let cell_id = $cell.data('cell_id');
                let cell_name = $cell.data('name');
                let subobject_id = $cell.data('subobject_id');
                let width = $cell.width() + 2;
                let height = $cell.height() + 2;
                let left = cellPosition.left - schemaPosition.left - planPosition.left;
                let top = cellPosition.top - schemaPosition.top - planPosition.top;


                $.ajax({
                    url: that.$editor_url + '/' + floor_id + '/' + cell_id + '/edit',
                    data: {
                        "place_floor_cell[name]": cell_name,
                        "place_floor_cell[subobject]": subobject_id,
                        "place_floor_cell[coord][0]": left,
                        "place_floor_cell[coord][1]": top,
                        "place_floor_cell[size][0]": width,
                        "place_floor_cell[size][1]": height
                    },
                    type: "POST",
                    beforeSend: function () {
                        $cell.addClass('element_loading');
                        $('#place_floor_cell_'+cell_id).parent().addClass('core_loading');
                    },
                    success: function (data) {
                        $cell.removeClass('element_loading');
                        $('#place_floor_cell_'+cell_id).parent().removeClass('core_loading');
                        if (data.result === 1) {
                            $('#place_floor_cell_'+cell_id+' input[name="place_floor_cell[coord][0]"]').val(left);
                            $('#place_floor_cell_'+cell_id+' input[name="place_floor_cell[coord][1]"]').val(top);
                            $('#place_floor_cell_'+cell_id+' input[name="place_floor_cell[size][0]"]').val(width);
                            $('#place_floor_cell_'+cell_id+' input[name="place_floor_cell[size][1]"]').val(height);
                        }
                    }
                });
            },

            deformingCell: function (e) {
                this.levels.updateLevelsHeight(this);
            },
        },

        levels: {
            createLevel: function(e) {
                e.stopImmediatePropagation();

                if (!$(e.currentTarget).hasClass('clicked')) {

                    let that = this;
                    let floor_id = $('#place_editor').data('floor_id');
                    let cell_id = $('#place_editor .cell_levels').data('cell_id');
                    let $cell = $('.cr_fg[data-cell_id="' + cell_id + '"]');
                    let level_name = parseInt($('#place_editor .cell_levels .level').length) + 1;

                    $.ajax({
                        url: that.$editor_url + '/' + floor_id + '/' + cell_id + '/new',
                        data: {'place_floor_cell_level[cell]': cell_id, 'place_floor_cell_level[name]': level_name},
                        type: "POST",
                        beforeSend: function () {
                            $cell.addClass('element_loading');
                            $(e.currentTarget).addClass('clicked');
                            $('#place_floor_cell_'+cell_id).parent().addClass('core_loading');
                        },
                        success: function (data) {
                            $cell.removeClass('element_loading');
                            $('#place_floor_cell_'+cell_id).parent().removeClass('core_loading');
                            $(e.currentTarget).removeClass('clicked');
                            if (data.result === 1) {
                                $cell.find('.cr_layer_box').append('<div class="i" data-level_id="'+data.id+'">'+data.name+'</div>');

                                $newLevel = $('#place_editor .cell_levels .level:last').clone();
                                $newLevel.attr('data-level_id', data.id);
                                $newLevel.find('input').val(data.name);
                                $newLevel.find('button').removeClass('disabled').addClass('delete_level');
                                $('#place_editor .cell_levels').append($newLevel);
                                that.levels.updateLevelsHeight(that);
                            }
                        }
                    });
                }
            },

            deleteLevel: function (e) {
                e.preventDefault();

                if (confirm('Вы уверены, что хотите удалить уровень?')) {

                    let that = this;
                    let floor_id = $('#place_editor').data('floor_id');
                    let cell_id = $(e.currentTarget).parents('.cell_levels').data('cell_id');
                    let level_id = $(e.currentTarget).parents('.level').data('level_id');
                    let $cell = $('.cr_fg[data-cell_id="' + cell_id + '"]');

                    $.ajax({
                        url: that.$editor_url + '/' + floor_id + '/' + cell_id + '/' + level_id + '/delete',
                        type: "DELETE",
                        beforeSend: function () {
                            $cell.addClass('element_loading');
                            $('#place_floor_cell_'+cell_id).parent().addClass('core_loading');
                        },
                        success: function (data) {
                            $cell.removeClass('element_loading');
                            $('#place_floor_cell_'+cell_id).parent().removeClass('core_loading');
                            if (data.result === 1) {
                                $cell.find('.cr_layer_box .i:last').remove();
                                $(e.currentTarget).parents('.cell_levels').find('.level[data-level_id="' + level_id + '"]').remove();
                                that.levels.updateLevelsHeight(that);
                            }
                        }
                    });
                }
            },

            saveLevel: function (e) {
                e.preventDefault();

                let that = this;
                let floor_id = $('#place_editor').data('floor_id');
                let cell_id = $(e.currentTarget).parents('.cell_levels').data('cell_id');
                let level_id = $(e.currentTarget).parents('.level').data('level_id');
                let $cell = $('.cr_fg[data-cell_id="' + cell_id + '"]');
                let $level = $('.cr_fg[data-cell_id="' + cell_id + '"]').find('.cr_layer_box .i[data-level_id="' + level_id + '"]');
                let level_name = $(e.currentTarget).val();

                $.ajax({
                    url: that.$editor_url + '/' + floor_id + '/' + cell_id + '/' + level_id + '/edit',
                    type: "POST",
                    data: {'place_floor_cell_level[cell]': cell_id, 'place_floor_cell_level[name]': level_name},
                    beforeSend: function () {
                        $cell.addClass('element_loading');
                        $('#place_floor_cell_'+cell_id).parent().addClass('core_loading');
                    },
                    success: function (data) {
                        $cell.removeClass('element_loading');
                        $('#place_floor_cell_'+cell_id).parent().removeClass('core_loading');
                        if (data.result === 0) {
                            for (let key in data.data) {
                                $(e.currentTarget).closest('div.form-group').addClass('has-error');
                                $(e.currentTarget).parents('.input-group').after('<span class="help-block"><ul class="list-unstyled"><li>'+data.data[key]+'</li></ul></span>');
                            }
                        } else {
                            $(e.currentTarget).closest('div.form-group').removeClass('has-error');
                            $(e.currentTarget).closest('div.form-group').find('span.help-block').remove();
                            $level.html(level_name);
                        }
                    }
                });
            },

            updateLevelsHeight: function(e) {
                $('#place_editor .cr_fg').each(function(i, cell) {
                    let levels = $(cell).find('.cr_layer_box .i').length;
                    let height = $(cell).height();
                    let level_height = parseInt(height/levels);
                    let cell_color = $(cell).css('border-color');
                    $(cell).find('.cr_layer_box .i').css('height', level_height);
                    $(cell).find('.cr_layer_box .i').css('line-height', level_height+'px');
                    $(cell).find('.cr_layer_box .i').css('border-color', cell_color);
                });
                $('#place_preview .cr_fg').each(function(i, cell) {
                    let levels = $(cell).find('.cr_layer_box .i').length;
                    let height = $(cell).height();
                    let level_height = parseInt(height/levels);
                    let cell_color = $(cell).css('border-color');
                    $(cell).find('.cr_layer_box .i').css('height', level_height);
                    $(cell).find('.cr_layer_box .i').css('line-height', level_height+'px');
                    $(cell).find('.cr_layer_box .i').css('border-color', cell_color);
                });
            }
        },

        zoom: {
            updateZoom: function(e) {
                let action_type = 0;
                if ($(e.currentTarget).hasClass('cr_scale_plus'))
                    action_type = 1;

                let $plan = $(e.currentTarget).parent().parent().find('.cr_figures_plan');

                var current_scale = $plan.attr('data-scale');
                if (current_scale == undefined)
                    current_scale = 1;
                else
                    current_scale = parseFloat(current_scale);

                if (action_type == 0)
                    current_scale -= 0.1;
                else
                    current_scale += 0.1;

                if (current_scale < 0.1)
                    current_scale = 0.1;

                if (current_scale > 1)
                    current_scale = 1;

                $plan.attr('data-scale', current_scale);

                $plan.css({
                    transform : 'scale('+ current_scale +')'
                });

                $plan.css({"-webkit-transform-origin": "0px 0px", "height": "inherit"});
            },
        },

        preview: {
            showFloor: function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                let that = this;
                let url = $(e.currentTarget).attr('href');
                let floor_editor_url = $(e.currentTarget).data('editor_url');

                $(e.currentTarget).parent().find('a').removeClass('active');
                $('#edit_plan_link').attr('href', floor_editor_url);

                $.ajax({
                    url: url,
                    type: "GET",
                    beforeSend: function () {
                        $('#place_preview .cr_schema').addClass('core_loading');
                        $(e.currentTarget).addClass('active');
                    },
                    success: function(data) {
                        $('#place_preview .cr_schema').removeClass('core_loading');
                        $('#place_preview').replaceWith($(data).find('#place_preview'));
                        that.levels.updateLevelsHeight(that);
                    }
                });
            },
        },

        order: {
            selectCell: function(e) {

                let $cell = $(e.currentTarget);
                let cell_id = $cell.data('cell_id');
                let date = $('#order_begin_at').val();
                let url = $('#order').data('order_url');

                $(e.currentTarget).toggleClass('selected');
                $(e.currentTarget).parents('.cr_cells').find('.cr_fg').not($(e.currentTarget)).removeClass('selected');
                if ($(e.currentTarget).hasClass('selected')) {
                    $.ajax({
                        url: url+cell_id+'/'+date,
                        type: "GET",
                        beforeSend: function () {
                            $('#order .col-xs-12').removeClass('col-xs-12').addClass('col-xs-9 b-r');
                            $('#order .cell_info').html('').css('height', '60vh').removeClass('core_hide').addClass('core_loading');
                        },
                        success: function (data) {
                            $('#order .cell_info').html($(data)).removeClass('core_loading');

                            let $items = $('#place_preview .cr_fg[data-cell_id='+cell_id+']').find('.cr_layer_box .i');
                            $items.each(function(i, item) {
                                let level_id = $(item).data('level_id');
                                if ($(item).hasClass('selected')) {
                                    $('#order .cell_items').find('.item[data-level_id='+level_id+'] input').attr('checked', true);
                                }
                                if ($(item).hasClass('reserved')) {
                                    $('#order .cell_items').find('.item[data-level_id='+level_id+'] input').attr('disabled', true);
                                    $('#order .cell_items').find('.item[data-level_id='+level_id+'] .status').html('Занят').addClass('text-danger');
                                }
                            });
                        }
                    });
                } else {
                    $('#order .col-xs-9').removeClass('col-xs-9 b-r').addClass('col-xs-12');
                    $('#order .cell_info').html('').addClass('core_hide');
                }
            },

            chooseItems: function(e) {
                let level_id = 0;
                let $items = $(e.currentTarget).parents('.cell_info').find('.item input');
                $items.each(function(i, item) {
                    level_id = $(item).val();
                    $('#rezerved_items').find('.item[data-level_id='+$(item).val()+']').remove();
                    $('#place_preview .cr_layer_box').find('.i[data-level_id='+level_id+']').removeClass('selected');
                    if ($(item).is(':checked')) {
                        let $item = $(item).parents('.item').clone();
                        $item.find('.status').html('');
                        $item.find('input').attr('type', 'hidden');
                        $item.find('input').attr('disabled', false);
                        if ($('#order').data('target') == 'request') {
                            $item.find('input').each(function () {
                                this.name = 'client_request[items][][cellLevel]';
                            })
                        }
                        $item.find('.remove_item').removeClass('core_hide');
                        $('#rezerved_items').append($item);
                        $('#place_preview .cr_layer_box').find('.i[data-level_id='+level_id+']').addClass('selected');
                        $('#order .form-errors').html('');
                    }
                });

                $('#place_preview .cr_layer_box').find('.i[data-level_id='+level_id+']').parents('.cr_fg').click();
            },

            removeItem: function(e) {
                let $item = $(e.currentTarget).parents('.item');
                let level_id = $(e.currentTarget).parents('.item').data('level_id');

                $item.remove();
                $('#place_preview .cr_layer_box').find('.i[data-level_id='+level_id+']').removeClass('selected');
            },

            showFloor: function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                let that = this;
                let begin_at = $('#order_begin_at').val();
                let end_at = $('#order_end_at').val();
                let url = $(e.currentTarget).attr('href');

                $(e.currentTarget).parent().find('a').removeClass('active');
                $('#place_preview .cr_fg.selected').click();

                $.ajax({
                    url: url+'/'+begin_at+'/'+end_at,
                    type: "GET",
                    beforeSend: function () {
                        $('#place_preview .cr_schema').addClass('core_loading');
                        $(e.currentTarget).addClass('active');
                    },
                    success: function(data) {
                        $('#place_preview .cr_schema').removeClass('core_loading');
                        $('#place_preview').replaceWith($(data).find('#place_preview'));
                        that.levels.updateLevelsHeight(that);

                        let $items = $('#rezerved_items').find('.item input');
                        $items.each(function(i, item) {
                            $('#place_preview .cr_layer_box').find('.i[data-level_id='+$(item).val()+']').addClass('selected');
                        });
                    }
                });
            },

            reloadFloor: function (e) {
                $('#order .show_floor_plan.active').click();
            },

            checkForm: function(e) {
                let items = $('#rezerved_items').find('.item');

                //$('#order .submit_btn').attr('disabled', true);
                // $('input[name="client_order[begin_at]"]').attr('readonly', false);
                // $('input[name="client_order[end_at]"]').attr('readonly', false);

                if (items.length != 0) {
                    //$('#order .submit_btn').attr('disabled', false);
                    // $('input[name="client_order[begin_at]"]').attr('readonly', true);
                    // $('input[name="client_order[end_at]"]').attr('readonly', true);
                }

                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            },

            changePlace: function(e) {
                let place_id = $(e.currentTarget).val();
                let url = '/admin/objects/places/'+place_id+'/orders/';

                if (place_id) {
                    $('#request_place_options').removeClass('hide');
                    $('#request .open_modal_plan').removeClass('disabled');
                    $('#order_begin_at').attr('readonly', false);
                    $('#order_end_at').attr('readonly', false);
                    $('#order').attr('data-order_url', url);
                    $('#place_floors a').each(function (i, v) {
                        let floor_url = $(v).attr('href');
                        floor_url = floor_url.replace(/places\/(\d)\/plan/g, 'places/' + place_id + '/plan');
                        $(v).attr('href', floor_url);
                    });
                    this.order.reloadFloor(e);

                    $('#rezerved_items').html('');
                    $('#order .place_plan').removeClass('hide');
                } else {
                    $('#request_place_options').addClass('hide');
                    $('#request .open_modal_plan').addClass('disabled');
                    $('#order_begin_at').attr('readonly', 'readonly');
                    $('#order_end_at').attr('readonly', 'readonly');
                }
            }
        },

        events: function(){
            this.$body.on('click', '#create_floor', this.floors.createFloor.bind(this));
            this.$body.on('click', '#delete_floor', this.floors.deleteFloor.bind(this));
            this.$body.on('click', '#cr_floors .floor', this.floors.loadFloor.bind(this));
            this.$body.on('click', '#place_editor .cr_upload_bg', this.floors.selectImage.bind(this));

            this.$body.on('click', '#place_editor .cr_navi .i', this.cells.selectTool.bind(this));
            this.$body.on('click', '#place_editor .cr_fg', this.cells.selectCell.bind(this));
            this.$body.on('click', '#place_editor .cr_schema', this.cells.createCell.bind(this));
            this.$body.on('change', '#place_editor select[name="place_floor_cell[subobject]"]', this.cells.changeCellType.bind(this));
            this.$body.on('click', '#place_editor .save_cell', this.cells.saveCell.bind(this));
            this.$body.on('click', '#place_editor .delete_cell', this.cells.deleteCell.bind(this));
            this.$body.on('drag resize', '#place_editor .cr_fg', this.cells.deformingCell.bind(this));
            this.$body.on('resizestop dragstop', '#place_editor .cr_fg', this.cells.deformCell.bind(this));

            this.$body.on('click', '#place_editor .cr_figures', this.cells.unselectCell.bind(this));

            this.$body.on('click', '#place_editor .add_level', this.levels.createLevel.bind(this));
            this.$body.on('click', '#place_editor .cell_levels .delete_level', this.levels.deleteLevel.bind(this));
            this.$body.on('change enterKey', '#place_editor .cell_levels input[name="place_floor_cell_level[name]"]', this.levels.saveLevel.bind(this));

            this.$body.on('click', '#place_editor .cr_scale_plus, #place_editor .cr_scale_minus', this.zoom.updateZoom.bind(this));
            this.$body.on('click', '#place_preview .cr_scale_plus, #place_preview .cr_scale_minus', this.zoom.updateZoom.bind(this));

            this.$body.on('click', '#plan .show_floor_plan', this.preview.showFloor.bind(this));

            this.$body.on('click', '#order .show_floor_plan', this.order.showFloor.bind(this));
            this.$body.on('click', '#order .cr_fg', this.order.selectCell.bind(this));
            this.$body.on('click', '#order .choose_items_btn', this.order.chooseItems.bind(this));
            this.$body.on('click', '#rezerved_items .remove_item', this.order.removeItem.bind(this));
            this.$body.on('click', '#order form', this.order.checkForm.bind(this));
            this.$body.on('keyup keypress', '#order form', this.order.checkForm.bind(this));
            this.$body.on('changeDate', '#order .datepicker, #request .datepicker', this.order.reloadFloor.bind(this));
            this.$body.on('change', '#request select[name="client_request[place]"]', this.order.changePlace.bind(this));
        },

        init: function () {
            this.cacheDom();
            this.events();

            this.levels.updateLevelsHeight(this);

            $('#place_editor .cr_fg').resizable({disabled: false, containment: ".cr_figures"});
            $('#place_editor .cr_fg').draggable({disabled: false, containment: ".cr_figures"});

            $('[data-toggle="tooltip"]').tooltip();
        }
    };

    placeEditor.init();

})(jQuery);