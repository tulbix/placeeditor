$('form.ajax-validation-form').submit(function(e) {
    e.preventDefault();
    let form_serialize = $(this).serialize(),
        form = $(e.currentTarget),
        action = (form.attr('action') !== undefined) ? form.attr('action') : location.href;

    $.ajax({
        url: action,
        data: form_serialize,
        type: "POST",
        success: function(data) {
            $('div.form-group').removeClass('has-error');
            $('div.form-group > span.help-block').remove();
            $(form).find('.form-errors').html('');
            if (data.result === 0) {
                for (let key in data.data) {
                    let current_field = $(form.find('[name*="'+key+'"]')[0]);
                    if (current_field.length) {
                        current_field.closest('div.form-group').addClass('has-error');
                        $(form.find('[name*="' + key + '"]')[0]).after('<span class="help-block"><ul class="list-unstyled"><li>' + data.data[key] + '</li></ul></span>');
                    } else {
                        $(form).find('.form-errors').addClass('has-error').append('<span class="help-block"><ul class="list-unstyled"><li>' + data.data[key] + '</li></ul></span>');
                    }
                }
            } else {
                form.unbind('submit').submit();
            }
        }
    });
});